#!/bin/bash

cd "${BASH_SOURCE%/*}" || exit

. ./config.sh
. ./graphiteConfig.sh

sizeMB=$(du -sm $1 | awk '{print $1}')

echo "${THIS_HOSTNAME}.folder.$2 ${sizeMB} `date +%s`" | nc -q 0 ${GRAPHITE_HOST} ${GRAPHITE_PORT}

