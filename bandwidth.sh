#!/bin/bash

#KiB/MiB/GiB/TiB

cd "${BASH_SOURCE%/*}" || exit

. ./config.sh
. ./graphiteConfig.sh

final=0
statString=`export LC_ALL=C;LANG=en_US.UTF-8 date +"%b '%y"`
current=`vnstat -m | grep "$statString" | awk '{print $9}'`
currentUnits=`vnstat -m | grep "$statString" | awk '{print $10}'`


case $currentUnits in
  "KB") final=$(echo "$current / 1048576" | bc) ;;
  "MB") final=$(echo "$current / 1024" | bc) ;;
  "GB") final=$current ;;
  "TB") final=$(echo "$current * 1024" | bc) ;;
   *) exit 1 ;;
esac

echo "vnstat.${THIS_HOSTNAME}.month.tx_gb $final `date +%s`" | nc -q 0 ${GRAPHITE_HOST} ${GRAPHITE_PORT}

