#!/bin/bash
# 1 param = what to find
# 2 param = file to find in
# 3 param = graphite stat-name (will host.wcl.stat-name)

cd "${BASH_SOURCE%/*}" || exit

. ./config.sh
. ./graphiteConfig.sh


count=$(grep -i $1 $2 | wc -l)

echo "${THIS_HOSTNAME}.wcl.$3 ${count} `date +%s`" | nc -q 0 ${GRAPHITE_HOST} ${GRAPHITE_PORT}

